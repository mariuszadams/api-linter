# Camara Quality on Demand API specifications

* This project will be used to prepare and sync Service APIs for the [“Quality on Demand” Camara API subproject](https://github.com/camaraproject/QualityOnDemand/tree/main/code/API_definitions).
* 2 testing steps in CI/CD: YAML Lint and Stoplight Spectral API test.
* testing currently limited to _qos*.yaml_ files.

<br/>

## _Camara description_
## Scope
* It provides the customer with the ability to:  
  * set quality for a mobile connection (e.g. required latency, jitter, bit rate)  
  * get notification if network cannot fulfill  
  * NOTE: The scope of this API family should be limited (at least at a first stage) to 4G and 5G.  

