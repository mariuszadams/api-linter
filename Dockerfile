##Use container from Docker Hub
FROM stoplight/spectral

##Add spectral config and files to test
ADD ./*.yaml ./

##if you need a minimal spectral config for OAS3:
#RUN echo "extends: spectral:oas" > .spectral.yaml

##show yaml files that were added during build
RUN ls -la

##this will run automatically the lint command on all yaml files and exit
ENTRYPOINT ["spectral","lint","-D","./*.yaml"]

##run this if you want to pass other commands to the container
#ENTRYPOINT ["/bin/sh","-c"]
